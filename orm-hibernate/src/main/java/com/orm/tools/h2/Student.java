package com.orm.tools.h2;

import lombok.Data;

@Data
public class Student {
    private int id;
    private String name;
    private int age;
}
