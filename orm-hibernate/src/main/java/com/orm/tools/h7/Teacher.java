package com.orm.tools.h7;

import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;


@Entity
//@Table(name="_Teacher")
@IdClass(TeachertPk.class)
public class Teacher {
    /*	private TeachertPk pk;*/
    private int id;
    private String name;
    private String title;
    private String age;
    private Date birthdate;
    private ZhiCheng zhicheng;

    //@Temporal(TemporalType.DATE)
    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    //@Column(name="aaa")
//@Transient
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //@Enumerated(EnumType.STRING)
    @Enumerated(EnumType.ORDINAL)
    public ZhiCheng getZhicheng() {
        return zhicheng;
    }

    public void setZhicheng(ZhiCheng zhicheng) {
        this.zhicheng = zhicheng;
    }
}
