package com.orm.tools.h7;

import lombok.Data;

@Data
public class Student {

    private StudentPk pk;

    private int age;

}
