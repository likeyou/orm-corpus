package com.orm.tools.h0;

import lombok.Data;

@Data
public class StudentPk implements java.io.Serializable {
    private int id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (o instanceof StudentPk) {
            StudentPk pk = (StudentPk) o;
            if (this.id == pk.getId() && this.name.equals(pk.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
