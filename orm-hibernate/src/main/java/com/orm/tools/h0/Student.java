package com.orm.tools.h0;

import lombok.Data;

@Data
public class Student {
    private int studentId;

    private int age;

}
