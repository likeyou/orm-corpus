package com.orm.tools.h0;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Husband {
    @Setter
    private int id;
    @Setter
    @Getter
    private String name;
    @Setter
    private Wife wife;

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    @OneToOne
    @JoinColumn(name = "wifeId")
    public Wife getWife() {
        return wife;
    }

}
