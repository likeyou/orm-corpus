package com.orm.tools.h0;

import lombok.Data;

@Data
public class StudentCard {
    private int id;
    private String num;
    private Student student;
}
