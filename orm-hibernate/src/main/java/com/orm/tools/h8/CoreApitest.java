package com.orm.tools.h8;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class CoreApitest {
    private static SessionFactory sf = null;

    @BeforeClass
    public static void beforeClass() {
        sf = new Configuration().configure()
                .buildSessionFactory();
    }

    @Test
    public void testTeacherSave() {
        Teacher t = new Teacher();
        t.setId(2);
        t.setName("6");
        t.setAge("66");
        t.setTitle("66");

        t.setBirthdate(new Date());
        t.setZhicheng(ZhiCheng.A);
        Session session = sf.getCurrentSession();
        session.beginTransaction();
        session.save(t);
        System.out.println(t.getId());
        session.getTransaction().commit();
        //session.close();
    }

    @Test
    public void testTeacherDelete() {
        Teacher t = new Teacher();
        t.setName("6");

        t.setAge("66");
        t.setTitle("666");

        t.setBirthdate(new Date());
        t.setZhicheng(ZhiCheng.A);
        Session session = sf.getCurrentSession();
        session.beginTransaction();
        session.save(t);
        session.getTransaction().commit();

        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        session2.delete(t);
        session2.getTransaction().commit();
        //session.close();
    }

    @Test
    public void testTeacherDelete2() {
        Teacher t = new Teacher();
        t.setId(1);
        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        session2.delete(t);
        session2.getTransaction().commit();
        //session.close();
    }

    @Test
    public void testLoad() {
        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        Teacher t = (Teacher) session2.load(Teacher.class, 2);
        System.out.println("1111111load" + t.getClass());
        System.out.println("000000000000000000load" + t.getName());
        session2.getTransaction().commit();

        //session.close();
    }

    @Test
    public void testGet() {
        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        Teacher t = (Teacher) session2.get(Teacher.class, 2);
        session2.getTransaction().commit();
        System.out.println("1111111get" + t.getClass());
        System.out.println("000000000000000000get" + t.getName());
        //session.close();
    }

    @Test
    public void testUpdate1() {
        Session session = sf.getCurrentSession();
        session.beginTransaction();
        Teacher t = (Teacher) session.get(Teacher.class, 2);
        t.setName("www.bai.du.com");

        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        session2.update(t);
        session.getTransaction().commit();


        //session.close();
    }


    @Test
    public void testUpdate2() {
        Teacher t = new Teacher();
        t.setId(0);
        t.setName("wwwweweweqom");

        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        session2.update(t);
        session2.getTransaction().commit();


        //session.close();
    }

    @Test
    public void testUpdate3() {
        Session session = sf.getCurrentSession();
        session.beginTransaction();
        Teacher t = (Teacher) session.get(Teacher.class, 2);
        t.setTitle("ooopppp");
        session.getTransaction().commit();
    }

    @Test
    public void testUpdate4() {
        Session session = sf.getCurrentSession();
        session.beginTransaction();
        Student s = (Student) session.get(Student.class, 2);
        s.setAge(199);
        session.update(s);
        session.getTransaction().commit();
    }

    @Test
    public void testUpdate5() {
        Session session = sf.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery(" update Teacher t set t.title='test' where id=0");
        query.executeUpdate();
        session.getTransaction().commit();


        //session.close();
    }

    @Test
    public void testClear() {
        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        Teacher t = (Teacher) session2.load(Teacher.class, 2);
        System.out.println("1111111load" + t.getName());
        session2.clear();
        Teacher t1 = (Teacher) session2.load(Teacher.class, 2);
        System.out.println("1111111load" + t1.getName());
        session2.getTransaction().commit();
    }

    @Test
    public void testSaveorUpdate() {

        Teacher t = new Teacher();
        t.setId(4);
        t.setName("6");

        t.setAge("66");
        t.setTitle("66");
        t.setBirthdate(new Date());

        Session session = sf.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(t);
        session.getTransaction().commit();

    }


    @Test
    public void testFlush() {

        Session session2 = sf.getCurrentSession();
        session2.beginTransaction();
        Teacher t = (Teacher) session2.load(Teacher.class, 2);
        t.setTitle("oooo");
        session2.flush();
        t.setTitle("ooooo");
        session2.getTransaction().commit();
    }


    @AfterClass
    public static void AfterClass() {
        sf.close();
    }


}
