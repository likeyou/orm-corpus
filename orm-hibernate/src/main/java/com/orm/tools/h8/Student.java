package com.orm.tools.h8;

import lombok.Data;

@Data
public class Student {
    private StudentPk pk;

    private int age;

}
