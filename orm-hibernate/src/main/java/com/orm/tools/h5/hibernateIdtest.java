package com.orm.tools.h5;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class hibernateIdtest {
	private static SessionFactory sf=null;
	@BeforeClass
	public static void beforeClass(){
		sf=new AnnotationConfiguration().configure().buildSessionFactory();
	}
@Test
public void testStudentSave(){
	Student s=new Student();
	s.setAge(8);
	s.setName("����8");
Session session=sf.openSession();
session.beginTransaction();
	session.save(s);
	session.getTransaction().commit();
	session.close();
}
@AfterClass
public static void AfterClass(){
	sf.close();
}
}
