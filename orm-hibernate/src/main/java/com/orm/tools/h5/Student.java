package com.orm.tools.h5;

import lombok.Data;

@Data
public class Student {
    private String id;
    private String name;
    private int age;


}
