package com.orm.tools.h5;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TeacherTest {
    private static SessionFactory sf = null;

    @BeforeClass
    public static void beforeClass() {
        sf = new Configuration().configure().buildSessionFactory();
    }

    @Test
    public void testTeacherSave() {
        Teacher t = new Teacher();
        t.setId(1);
        t.setAge("33");
        t.setTitle("2");
        t.setName("2");
        t.setBirthdate(new Date());
        t.setZhicheng(ZhiCheng.A);
        Session session = sf.openSession();
        session.beginTransaction();
        session.save(t);
        session.getTransaction().commit();
        session.close();
    }

    @AfterClass
    public static void AfterClass() {
        sf.close();
    }
}
