package com.orm.tools.h9;

import lombok.Data;

@Data
public class StudentCard {
    private int id;
    private String num;
    private Student student;
}
