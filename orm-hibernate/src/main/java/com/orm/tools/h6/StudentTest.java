package com.orm.tools.h6;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class StudentTest {


    private static SessionFactory sf = null;

    @BeforeClass
    public static void beforeClass() {
        sf = new Configuration().configure().buildSessionFactory();
    }

    @Test
    public void testStudentSave() {
        StudentPk pk = new StudentPk();

        Student s = new Student();
        pk.setId(3);
        pk.setName("wangwu");
        s.setPk(pk);
        s.setAge(8);

        Session session = sf.openSession();
        session.beginTransaction();
        session.save(s);
        session.getTransaction().commit();
        session.close();
    }

    @AfterClass
    public static void AfterClass() {
        sf.close();
    }

}
