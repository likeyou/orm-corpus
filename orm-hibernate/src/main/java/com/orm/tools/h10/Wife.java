package com.orm.tools.h10;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(WifePK.class)
public class Wife {
    private int id;
    private String name;
    private int age;

    public int getAge() {
        return age;
    }

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    @Id
    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

}
