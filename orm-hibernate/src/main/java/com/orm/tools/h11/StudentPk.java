package com.orm.tools.h11;

public class StudentPk implements java.io.Serializable {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof StudentPk) {
            StudentPk pk = (StudentPk) o;
            if (this.id == pk.getId() && this.name.equals(pk.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();

    }
}
