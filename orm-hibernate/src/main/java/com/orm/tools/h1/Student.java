package com.orm.tools.h1;

import lombok.Data;

@Data
public class Student {
    private int id;
    private String name;
    private int age;

}
