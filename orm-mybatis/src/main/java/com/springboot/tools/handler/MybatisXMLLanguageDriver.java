package com.springboot.tools.handler;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;

public class MybatisXMLLanguageDriver extends XMLLanguageDriver
{
    @Override
    public ParameterHandler createParameterHandler(
            MappedStatement mappedStatement, Object parameterObject,
            BoundSql boundSql)
    {
        /* ʹ���Զ��� ParameterHandler */
        return new MybatisDefaultParameterHandler(mappedStatement,
                parameterObject, boundSql);
    }
}
