package com.springboot.tools.generator;


import com.springboot.tools.annotations.IdType;

public class ConfigGenerator
{
    private String saveDir;
    private String entityPackage;
    private String mapperPackage;
    // db_config
    private boolean dbPrefix = false;
    private String dbDriverName;
    private String dbUser;
    private String dbPassword;
    private String dbUrl;
    private IdType idType = null;

    public String getSaveDir()
    {
        return saveDir;
    }

    public void setSaveDir(String saveDir)
    {
        this.saveDir = saveDir;
    }

    public String getEntityPackage()
    {
        return entityPackage;
    }

    public void setEntityPackage(String entityPackage)
    {
        this.entityPackage = entityPackage;
    }

    public String getMapperPackage()
    {
        return mapperPackage;
    }

    public void setMapperPackage(String mapperPackage)
    {
        this.mapperPackage = mapperPackage;
    }

    public boolean isDbPrefix()
    {
        return dbPrefix;
    }

    public void setDbPrefix(boolean dbPrefix)
    {
        this.dbPrefix = dbPrefix;
    }

    public String getDbDriverName()
    {
        return dbDriverName;
    }

    public void setDbDriverName(String dbDriverName)
    {
        this.dbDriverName = dbDriverName;
    }

    public String getDbUser()
    {
        return dbUser;
    }

    public void setDbUser(String dbUser)
    {
        this.dbUser = dbUser;
    }

    public String getDbPassword()
    {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword)
    {
        this.dbPassword = dbPassword;
    }

    public String getDbUrl()
    {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl)
    {
        this.dbUrl = dbUrl;
    }

    public IdType getIdType()
    {
        return idType;
    }

    public void setIdType(IdType idType)
    {
        this.idType = idType;
    }

}
