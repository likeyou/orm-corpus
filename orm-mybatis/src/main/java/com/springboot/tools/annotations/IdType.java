package com.springboot.tools.annotations;

public enum IdType
{
    AUTO("0", "0 "),

    ID_WORKER("1", "1 "),

    INPUT("2", " 2");

    private final String key;

    private final String desc;

    IdType(final String key, final String desc)
    {
        this.key = key;
        this.desc = desc;
    }

    public String getKey()
    {
        return key;
    }

    public String getDesc()
    {
        return desc;
    }

}
