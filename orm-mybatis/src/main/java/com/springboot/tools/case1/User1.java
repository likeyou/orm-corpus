package com.springboot.tools.case1;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User1 {
    private Integer id;
    private String name = null;

    public User1() {
    }

    public User1(int id, String name) {
        this.id = id;
        this.name = name;
    }


}
