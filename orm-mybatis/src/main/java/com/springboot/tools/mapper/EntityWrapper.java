package com.springboot.tools.mapper;

import com.springboot.tools.exceptions.MybatisPlusException;

public class EntityWrapper<T> {

    private T entity = null;

    private String orderByField = null;

    protected EntityWrapper() {
    }

    public EntityWrapper(T entity, String orderByField) {
        this.setEntity(entity);
        this.setOrderByField(orderByField);
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public String getOrderByField() {
        if (this.orderByField != null) {
            StringBuffer ob = new StringBuffer(" ORDER BY ");
            ob.append(this.orderByField);
            return ob.toString();
        }
        return null;
    }

    public void setOrderByField(String orderByField) {
        if (orderByField != null && !"".equals(orderByField)) {
            String ob = orderByField.toLowerCase();
            if (ob.contains("INSERT") || ob.contains("DELETE")
                    || ob.contains("UPDATE") || ob.contains("SELECT")) {
                throw new MybatisPlusException(" orderBy=[" + orderByField
                        + "], There may be SQL injection");
            } else {
                this.orderByField = orderByField;
            }
        }
    }

}
