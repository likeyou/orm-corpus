package com.springboot.tools.mapper;

public enum SqlMethod
{

    INSERT_ONE("insert", "����һ������",
            "<script>INSERT INTO %s %s VALUES %s</script>"), INSERT_BATCH(
            "insertBatch",
            "������������",
            "<script>INSERT INTO %s %s VALUES \n<foreach item=\"item\" index=\"index\" collection=\"list\" separator=\",\">%s\n</foreach></script>"),

    DELETE_BY_ID("deleteById", "����ID ɾ��һ������", "DELETE FROM %s WHERE %s=#{%s}"), DELETE_SELECTIVE(
            "deleteSelective", "���� entity ����ɾ����¼",
            "<script>DELETE FROM %s %s</script>"), DELETE_BATCH(
            "deleteBatchIds", "����ID���ϣ�����ɾ������",
            "<script>DELETE FROM %s WHERE %s IN (%s)</script>"),

    UPDATE_BY_ID("updateById", "����ID �޸�����", "<script>UPDATE %s %s</script>"),

    SELECT_BY_ID("selectById", "����ID ��ѯһ������",
            "SELECT %s FROM %s WHERE %s=#{%s}"), SELECT_BATCH("selectBatchIds",
            "����ID���ϣ�������ѯ����",
            "<script>SELECT %s FROM %s WHERE %s IN (%s)</script>"), SELECT_ONE(
            "selectOne", "��ѯ��������һ������", "<script>SELECT %s FROM %s %s</script>"), SELECT_LIST(
            "selectList", "��ѯ����������������", "<script>SELECT %s FROM %s %s</script>");

    private final String method;

    private final String desc;

    private final String sql;

    SqlMethod(final String method, final String desc, final String sql)
    {
        this.method = method;
        this.desc = desc;
        this.sql = sql;
    }

    public String getMethod()
    {
        return this.method;
    }

    public String getDesc()
    {
        return this.desc;
    }

    public String getSql()
    {
        return this.sql;
    }

}
