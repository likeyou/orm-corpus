package com.springboot.tools.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface BaseMapper<T, I> {

    int insert(T entity);


    int insertBatch(List<T> entityList);


    int deleteById(I id);


    int deleteSelective(T entity);

    int deleteBatchIds(List<I> idList);

    int updateById(T entity);

    T selectById(I id);

    List<T> selectBatchIds(List<I> idList);

    T selectOne(T entity);

    List<T> selectList(RowBounds rowBounds, @Param("ew")
    EntityWrapper<T> entityWrapper);

}
