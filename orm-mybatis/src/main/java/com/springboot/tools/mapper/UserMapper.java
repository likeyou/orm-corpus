package com.springboot.tools.mapper;



import com.springboot.tools.entity.User;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

public interface UserMapper extends AutoMapper<User> {

    List<User> selectListRow(RowBounds pagination);

}
