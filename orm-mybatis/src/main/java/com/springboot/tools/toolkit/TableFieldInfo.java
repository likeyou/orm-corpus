package com.springboot.tools.toolkit;

public class TableFieldInfo

{
    // �Ƿ�����ֶ���������������
    private boolean related;
    // �ֶ���

    private String column;
    // ������
    private String property;

    public boolean isRelated()
    {
        return related;
    }

    public void setRelated(boolean related)
    {
        this.related = related;
    }

    public String getColumn()
    {
        return column;
    }

    public void setColumn(String column)
    {
        this.column = column;
    }

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public TableFieldInfo(String column)
    {
        super();
        this.column = column;
    }

    public TableFieldInfo(boolean related, String column, String property)
    {
        super();
        this.related = related;
        this.column = column;
        this.property = property;
    }

}
