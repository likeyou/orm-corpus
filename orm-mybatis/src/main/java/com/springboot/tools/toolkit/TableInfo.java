package com.springboot.tools.toolkit;

import com.springboot.tools.annotations.IdType;

import java.util.List;

public class TableInfo
{

    private IdType idType;

    private String tableName;

    public IdType getIdType()
    {
        return idType;
    }

    public void setIdType(IdType idType)
    {
        this.idType = idType;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public boolean isKeyRelated()
    {
        return keyRelated;
    }

    public void setKeyRelated(boolean keyRelated)
    {
        this.keyRelated = keyRelated;
    }

    public String getKeyProperty()
    {
        return keyProperty;
    }

    public void setKeyProperty(String keyProperty)
    {
        this.keyProperty = keyProperty;
    }

    public String getKeyColumn()
    {
        return keyColumn;
    }

    public void setKeyColumn(String keyColumn)
    {
        this.keyColumn = keyColumn;
    }

    public List<TableFieldInfo> getFieldList()
    {
        return fieldList;
    }

    public void setFieldList(List<TableFieldInfo> fieldList)
    {
        this.fieldList = fieldList;
    }

    // �����Ƿ�����ֶ���������������
    private boolean keyRelated = false;
    // ������id������
    private String keyProperty;
    // ������id�ֶ���
    private String keyColumn;
    // ���ֶ���Ϣ�б�
    private List<TableFieldInfo> fieldList;

}
