package com.springboot.tools.entity;

import com.springboot.tools.annotations.TableField;
import com.springboot.tools.annotations.TableId;
import com.springboot.tools.annotations.TableName;

import java.io.Serializable;
import java.lang.reflect.Field;

@TableName(value = "user")
public class User implements Serializable {


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;


    @TableId(value = "test_id")
    private Long id;

    private String name;

    private Integer age;

    @TableField(value = "test_type")
    private Integer testType;

    public User() {

    }

    public User(String name) {
        this.name = name;
    }

    public User(Integer testType) {
        this.testType = testType;
    }

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(Long id, Integer age) {
        this.id = id;
        this.age = age;
    }

    public User(Long id, String name, Integer age, Integer testType) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.testType = testType;
    }

    public User(String name, Integer age, Integer testType) {
        this.name = name;
        this.age = age;
        this.testType = testType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getTestType() {
        return testType;
    }

    public void setTestType(Integer testType) {
        this.testType = testType;
    }

    public static void main(String args[]) throws IllegalAccessException {
        User user = new User();
        user.setName("12306");
        user.setAge(3);
        System.err.println(User.class.getName());
        Field[] fields = user.getClass().getDeclaredFields();
        for (Field field : fields) {
            System.out.println("===================================");
            System.out.println(field.getName());
            System.out.println(field.getType().toString());
            field.setAccessible(true);
            System.out.println(field.get(user));
        }
    }
}
